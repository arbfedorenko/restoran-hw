"""This unit transforms days and time when restaurant is open"""
"""into understandable format"""
from fastapi import Depends, FastAPI, Body, HTTPException
from pydantic import BaseModel, validator
from fastapi.responses import JSONResponse
from app.configuration import settings
from fastapi.encoders import jsonable_encoder

app = FastAPI()


class TimeOfRestoran(BaseModel):
    """This class crated for control the input if the data is correct"""

    type: str
    value: int

    @validator("type")
    @classmethod
    def validate_type(cls, v: str) -> str:
        """validator"""
        if not ((v == "open") or (v == "close")):
            raise HTTPException(status_code=404, detail=str(v) + " is not correct type")
        return v

    @validator("value")
    @classmethod
    def validate_value(cls, v: int) -> int:
        """validator 2"""
        if not ((0 <= v) and (v <= 24 * 3600 - 1)):
            raise HTTPException(status_code=404, detail=str(v) + " is not correct time")
        return v


class RestoranTime(BaseModel):
    """Restourant time"""

    monday: list[TimeOfRestoran] | None = None
    tuesday: list[TimeOfRestoran] | None = None
    wednesday: list[TimeOfRestoran] | None = None
    thursday: list[TimeOfRestoran] | None = None
    friday: list[TimeOfRestoran] | None = None
    saturday: list[TimeOfRestoran] | None = None
    sunday: list[TimeOfRestoran] | None = None


def get_a_day_name(day_number: int) -> str:
    """Returns the day Name in dependence of it's number"""
    if day_number == 1:
        return "Monday"
    elif day_number == 2:
        return "Tuesday"
    elif day_number == 3:
        return "Wednesday"
    elif day_number == 4:
        return "Thursday"
    elif day_number == 5:
        return "Friday"
    elif day_number == 6:
        return "Saturday"
    elif day_number == 7:
        return "Sunday"


def get_a_day_number(day_name: str) -> int:
    """Returns the day number by it's name"""
    l_day_name = day_name.capitalize()
    if l_day_name == "Monday":
        return 1
    elif l_day_name == "Tuesday":
        return 2
    elif l_day_name == "Wednesday":
        return 3
    elif l_day_name == "Thursday":
        return 4
    elif l_day_name == "Friday":
        return 5
    elif l_day_name == "Saturday":
        return 6
    elif l_day_name == "Sunday":
        return 7
    else:
        raise HTTPException(status_code=404, detail=day_name + " is a wrong day of week")


def get_a_day(sec: int) -> int:
    """Returns the day number from the time in seconds"""

    return sec // (3600 * 24) + 1


def get_a_time(sec: int) -> str:
    """from the time in seconds obtains string hr:min AM or PM"""

    l_sec = sec % (3600 * 24)
    hour = l_sec // 3600
    min = (l_sec % 3600) // 60
    r_hour = 0
    s_min = ""
    s_hour = ""
    r_min = 0
    r_ampm = ""
    if hour == 0:
        r_min = min
        r_hour = 12
        r_ampm = "AM"
    if (hour >= 1) and (hour <= 11):
        r_min = min
        r_hour = hour
        r_ampm = "AM"
    if hour == 12:
        r_min = min
        r_hour = hour
        r_ampm = "PM"
    if (hour >= 13) and (hour <= 23):
        r_min = min
        r_hour = hour - 12
        r_ampm = "PM"
    if r_min == 0:
        s_min = "00"
    else:
        s_min = str(r_min)
    if r_hour == 0:
        s_hour = "00"
    else:
        s_hour = str(r_hour)

    return s_hour + ":" + s_min + " " + r_ampm


def when_restourant_is_closed(slovar_days: dict[str, list], all_day_closed: dict, days: list) -> None:
    """Here we find the days when restourant is closed"""
    # выберем дни когда ресторан закрыт целый день
    for i in range(1, 8):

        dayname = get_a_day_name(i).lower()
        l_restoran_time = slovar_days[dayname]
        if l_restoran_time != None:
            if isinstance(l_restoran_time, list):
                if len(l_restoran_time) == 0:
                    all_day_closed[dayname] = 1


def transform_shedule_to_sec_from_monday(slovar_days: dict[str, list], all_day_closed: dict, days: list) -> None:
    """transform shedule to sec from monday"""

    for i in range(1, 8):
        dayname = get_a_day_name(i).lower()
        l_restoran_time = slovar_days[dayname]
        if l_restoran_time != None:

            if isinstance(l_restoran_time, list):
                if len(l_restoran_time) > 0:
                    # Если ресторан был закрыт и вдруг есть open или close ошибка
                    if all_day_closed.get(dayname) == 1:
                        raise HTTPException(
                            status_code=404, detail="This day " + dayname + " is already marked as closed"
                        )
                    for times in l_restoran_time:
                        tr = TimeOfRestoran(type=times.type, value=times.value)
                        days.append([times.value + 3600 * 24 * (i - 1), times.type])
                        # raise HTTPException(status_code = 404, detail=str(times.value)+" "+str(times.type))
    days = sorted(days, key=lambda time: time[0])


def processing(restoran: RestoranTime) -> dict:
    """Here we transform input data to readable format"""
    slovar_days = {
        "monday": restoran.monday,
        "tuesday": restoran.tuesday,
        "wednesday": restoran.wednesday,
        "thursday": restoran.thursday,
        "friday": restoran.friday,
        "saturday": restoran.saturday,
        "sunday": restoran.sunday,
    }

    all_day_closed : dict[str,int]= {}
    shed: dict[int, list] = {}
    days: list = []
    days_dict_output = {}
    days = sorted(days, key=lambda time: time[0])

    when_restourant_is_closed(slovar_days, all_day_closed, days)

    transform_shedule_to_sec_from_monday(slovar_days, all_day_closed, days)
    # если все началось с закрытия - это может от вечера воскресения закрытие - в конец его
    if len(days) > 0:
        if days[0][1] == "close":
            days[0][0] = days[0][0] + 7 * 3600 * 24
    days = sorted(days, key=lambda time: time[0])
    if len(days) > 0 and get_a_day(days[len(days) - 1][0]) > 8:
        raise HTTPException(status_code=404, detail="Wrong shedule value")
    l_index = 0
    the_day = 0
    if len(days) % 2 != 0:
        raise HTTPException(status_code=404, detail="Amount of openig not equal amount of close")
    while l_index < len(days):
        print(str(l_index))
        if days[l_index][1] != "open":
            raise HTTPException(status_code=404, detail="wrong open close sequence")
        if days[l_index + 1][1] != "close":
            raise HTTPException(status_code=404, detail="wrong open close sequence")
        if abs(get_a_day(days[l_index + 1][0]) - get_a_day(days[l_index + 1][0])) > 1:
            raise HTTPException(status_code=404, detail="The time between open and close is too large")
        the_day = get_a_day(days[l_index][0])
        if shed.get(the_day) is None:
            shed[the_day] = []

        shed[the_day].append(get_a_time(days[l_index][0]) + "-" + get_a_time(days[l_index + 1][0]))
        l_index += 2
    for i_day in range(1, 8):
        if all_day_closed.get(get_a_day_name(i_day).lower()) == 1:
            days_dict_output[get_a_day_name(i_day).lower()] = "closed"
        elif not (shed.get(i_day) is None):
            days_dict_output[get_a_day_name(i_day).lower()] = ", ".join(shed[i_day])
    return days_dict_output


@app.post("/")
async def rest_time(restoran: RestoranTime) -> "JSONResponse":
    """post method implementation to transform input restourant shedele"""
    """into the normal format"""
    # processing(restoran)

    json_compatible_item_data = jsonable_encoder(processing(restoran))
    return JSONResponse(content=json_compatible_item_data)
    # return {processing(restoran)}
