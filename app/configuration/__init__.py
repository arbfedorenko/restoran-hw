from pydantic import BaseSettings


class Settings(BaseSettings):
    WORKERS: int
    PORT: int

    class Config:
        env_file = ".env"


settings = Settings()
