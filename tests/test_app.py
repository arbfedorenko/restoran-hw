"""This unit is for tests"""
import json

from app.app import TimeOfRestoran, get_a_day_name, get_a_day_number, processing, rest_time
from app.app import app

from fastapi import Body, Depends, FastAPI, HTTPException
from fastapi.testclient import TestClient

import pytest


from pytest_mock import MockFixture

client = TestClient(app)


def test_get_a_day_name() -> None:
    """get a day by the number 1 - Monday"""
    assert get_a_day_name(1) == "Monday"


# def test_get_a_day_name_exc(mocker: MockFixture):
#     mocker.patch.object(app.app, "HTTPException", return_value=HTTPException())
#     assert get_a_day_name(1)==404
def test_get_a_day_number_wr() -> None:
    """Get a day number by the day name when the day name is wrong"""
    with pytest.raises(HTTPException) as ex:
        get_a_day_number("Mondays")
    assert ex.value.detail == "Mondays is a wrong day of week"


def test_get_a_day_number_ok() -> None:
    """Get a day number by the day name when the day name is ok"""
    assert get_a_day_number("Monday") == 1


def test_TimeOfRestoran_type() -> None:
    """When the type is not open and not close the error must occur"""
    with pytest.raises(HTTPException) as ex:
        TimeOfRestoran(type="op", value=0)
    assert ex.value.detail == "op is not correct type"


def test_TimeOfRestoran_time_nok() -> None:
    """The tyme for open may be only in 24 hrs"""
    with pytest.raises(HTTPException) as ex:
        TimeOfRestoran(type="open", value=86401)
    assert ex.value.detail == "86401 is not correct time"


def test_TimeOfRestoran_time_close_ok() -> None:
    """time of close can not be more than 24 hrs"""
    tor = TimeOfRestoran(type="close", value=24 * 3600 - 1)
    assert tor.value == 86399


def test_rest_time() -> None:
    """query with impossible time for open"""
    l_str = json.loads('{"monday":[], "friday" : [{"type" : "open","value" : 6480000}]}')

    response = client.post("/", json=l_str)

    assert response.status_code == 404
    assert response.json() == {"detail": "6480000 is not correct time"}


def test_rest_time_ok() -> None:
    """query comparation answer with correct result"""
    l_str = json.loads(
        '{"monday":[],"friday" : [{"type" : "open","value" : 45380'
        + '}],"saturday": [{"type" : "close","value" : 3600},{"type" : "open",'
        + '"value" : 32400},{"type" : "close","value" : 39600},{"type" : "open",'
        + '"value" : 57600},{"type" : "close","value" : 82800}]}'
    )

    response = client.post("/", json=l_str)
    # response = client.get("/", headers={"X-Token": "coneofsilence"})
    assert response.status_code == 200
    compare_response = (
        '{  "monday": "closed",  "friday": "12:36 PM-1:00 AM",' + '"saturday": "9:00 AM-11:00 AM, 4:00 PM-11:00 PM"}'
    )
    compare_response = json.loads(compare_response)
    assert response.json() == compare_response


# def test_do_slow_method_function(mocker: MockFixture):
#     obj = App(2)
#     mocker.patch.object(app.application, "slow_function", return_value=2)
#     assert obj.do_slow_method_function() == 4
# class SlowClassMock:
#     def slow_method(self, x: int) -> int:
#         return x


# def test_do_slow_method_inner_method():
#     obj = App(2)
#     assert obj.do_slow_method_inner_method() == 4
