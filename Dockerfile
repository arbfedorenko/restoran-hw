FROM python:3.10.5-slim-buster
WORKDIR /application
COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt
COPY . .
CMD ["python", "./run.py"]
