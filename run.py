"""This unit runs restaurant app"""
from app.configuration import settings

import uvicorn

if __name__ == "__main__":

    uvicorn.run("app.app:app", host="0.0.0.0", port=settings.PORT, workers=settings.WORKERS, reload=True)
